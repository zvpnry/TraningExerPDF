﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainningPdf
{
    public interface ICommand
    {
        void Run();
        string GetDescription();
    }
}
