﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainningPdf.Exercicios;
using TrainningPdf.Math;
using TrainningPdf.Models;

namespace TrainningPdf
{
    interface IMenu
    {
        void Start();
    }

    public class BLMenu : IMenu
    {
        int spaces = 50;
        private List<Menu> lstMenus = new List<Menu>();

        public BLMenu()
        {
            InitializeMenu();
        }

        public void Start()
        {
            while (true)
            {
                PrintMenu();
                try
                {
                    var opcaoSelecionada = OpcaoSelecionada();
                    if (opcaoSelecionada.Number == 0)
                        break;
                    if (opcaoSelecionada.Command != null)
                    {
                        Console.WriteLine("+".PadRight(spaces, '-'));
                        Console.WriteLine(opcaoSelecionada.Command.GetDescription());
                        Console.WriteLine("+".PadRight(spaces, '-'));
                        Console.WriteLine();
                        opcaoSelecionada.Command.Run();
                        Console.WriteLine();
                    }
                    else
                        Console.WriteLine("Opção não implementada");
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Opção não implementada, exception");
                    Console.Write("Pretende mostrar a execpção (S/N)? ");
                    if (Console.ReadLine().ToUpper() == "S")
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadKey();
                    }
                }
            }
        }

        private void InitializeMenu()
        {
            lstMenus.Add(new Menu() { Nome = "Efectuar soma", Number = 1,Command = new Ex1()});
            lstMenus.Add(new Menu() { Nome = "Calculadora", Number = 2, Command = new Ex2() });
            lstMenus.Add(new Menu() { Nome = "Números pares", Number = 3, Command = new Ex3() });
            lstMenus.Add(new Menu() { Nome = "Números impares", Number = 4, Command = new Ex4() });
            lstMenus.Add(new Menu() { Nome = "Colocar em Maiusculas", Number = 5, Command = new Ex5() });
            lstMenus.Add(new Menu() { Nome = "Obter o menor numero", Number = 6, Command = new Ex6() });
            lstMenus.Add(new Menu() { Nome = "Obter o maior numero", Number = 7, Command = new Ex7() });
            lstMenus.Add(new Menu() { Nome = "Quit", Number = 0 });
        }
        private void PrintMenu()
        {
            
            int number_Sapces = 3;            
            if (spaces % 2 != 0)
                spaces++;        
            Console.WriteLine("+".PadRight((spaces - 4) / 2, '-') + "Menu" + "+".PadLeft((spaces - 4) / 2, '-'));
            foreach (Menu item in lstMenus)
            {
                Console.WriteLine("|"+ item.Number.ToString().PadLeft(number_Sapces)+ " - " + item.Nome.PadRight(spaces-2-number_Sapces -3) +"|");
            }
            Console.WriteLine("+".PadRight(spaces-1,'-') + "+");
        }

        private Menu OpcaoSelecionada()
        {
            Menu result = null;
            while (result == null)
            {
                Console.Write("Indique o numero da opção que pretende executar: ");
                int opcao;
                if(int.TryParse(Console.ReadLine(), out opcao))
                    result = lstMenus.FirstOrDefault(o => o.Number == opcao);
            }
            return result;
        }
    }
}
