﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainningPdf.Models
{
    public class Menu
    {
        public int Number { get; set; }
        public string Nome { get; set; }

        public ICommand Command { get; set; }

    }
}
