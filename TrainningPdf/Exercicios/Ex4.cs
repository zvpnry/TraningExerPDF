﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainningPdf.Math;

namespace TrainningPdf.Exercicios
{
    internal class Ex4 : ICommand
    {
        public string GetDescription()
        {
            return "Ex-4 - Escrever um algoritmo que gera e escreve os números ímpares entre 100 e 200.";
        }

        public void Run()
        {

            ConsoleRequest.EscreveListaDeNumeros(UtilsMath.ParOuImpares(100, 200, NumberType.impares));
            
        }

       
    }
}
