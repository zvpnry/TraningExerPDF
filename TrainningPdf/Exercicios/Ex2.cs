﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainningPdf.Math;

namespace TrainningPdf.Exercicios
{
    internal class Ex2 : ICommand
    {
        public string GetDescription()
        {
            return "Ex-2 - Construa um programa que leia dois números e um operador e efectue a operação introduzida.";
        }

        public void Run()
        {
            ICalculate calculator = new Calculate();
            ConsoleRequest.ShowResultado(calculator.Calcula(ConsoleRequest.LerNumero(1), ConsoleRequest.LerNumero(2), ConsoleRequest.LerOperadorMath()));
        }

       
    }
}
