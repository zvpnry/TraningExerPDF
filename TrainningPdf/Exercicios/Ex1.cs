﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainningPdf.Math;

namespace TrainningPdf.Exercicios
{
    internal class Ex1 : ICommand
    {
        public string GetDescription()
        {
            return "Ex-1 - Elabore o programa que leia dois números e efectue a sua soma, mostrando o resultado no ecrã.";
        }

        public void Run()
        {
            ICalculate calculator = new Calculate();
            ConsoleRequest.ShowResultado(calculator.Calcula(ConsoleRequest.LerNumero(1), ConsoleRequest.LerNumero(2),EnumOperadores.soma));
        }

       
    }
}
