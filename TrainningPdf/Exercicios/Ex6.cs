﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainningPdf.Math;

namespace TrainningPdf.Exercicios
{
    internal class Ex6 : ICommand
    {
        public string GetDescription()
        {
            return "Ex-6 -Escrever um algoritmo que leia dois valores e imprima o menor dos dois.";
        }

        public void Run()
        {
            int values = ConsoleRequest.RequestIntNumber("Quantos numeros pretende validar? ");
            List<decimal> listValues = new List<decimal>();
            for (int i = 0; i < values; i++)
            {
                listValues.Add(ConsoleRequest.LerNumero(i + 1));
            }
            ConsoleRequest.ShowResultado(UtilsMath.GetMinNumber(listValues));            
        }

       
    }
}
