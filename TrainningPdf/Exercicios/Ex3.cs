﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainningPdf.Math;

namespace TrainningPdf.Exercicios
{
    internal class Ex3 : ICommand
    {
        public string GetDescription()
        {
            return "Ex-3 - Elabore um programa que liste os números pares entre 1 e 100.";
        }

        public void Run()
        {

            ConsoleRequest.EscreveListaDeNumeros(UtilsMath.ParOuImpares(1, 100, NumberType.pares));
            
        }

       
    }
}
