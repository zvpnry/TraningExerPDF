﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainningPdf.Math;

namespace TrainningPdf.Exercicios
{
    internal class Ex5 : ICommand
    {
        public string GetDescription()
        {
            return "Ex-5 - Escreva um programa que converta o texto escrito em minúsculas para o correspondente em maiúsculas.";
        }

        public void Run()
        {
            ConsoleRequest.ShowResultado(ConsoleRequest.RequestInput("Escreva o texto que pretende converter em letras Maiusculas: ").ToUpper());            
        }

       
    }
}
