﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainningPdf
{
    public enum EnumOperadores
    {
        soma,
        divisao,
        multiplicacao,
        subtracao
    }

    public static class ConsoleRequest
    {
        
        public static decimal LerNumero()
        {
            return LerNumero(null);
        }

        public static decimal LerNumero(int? posicao)
        {

            return GetLerNumeroDecimal(string.Format("Escreva {0} numero: ", (posicao == null ? "um" : posicao.ToString() + "º")));
        }

        public static decimal RequestDecimalNumber(string message)
        {
            return GetLerNumeroDecimal(message);
        }

        public static int RequestIntNumber(string message)
        {
            return GetLerNumeroInt(message);
        }


        public static string RequestInput(string text)
        {
            Console.Write(text);
            return Console.ReadLine();
        }

        private static decimal GetLerNumeroDecimal(string message)
        {
            decimal result = 0;
            while (true)
            {
                var input = RequestInput(message);
                if (decimal.TryParse(input, out result))
                    break;
            }
            return result;
        }

        private static int GetLerNumeroInt(string message)
        {
            int result = 0;
            while (true)
            {
                var input = RequestInput(message);
                if (int.TryParse(input, out result))
                    break;
            }
            return result;
        }


        public static void ShowResultado(object resultado)
        {
            Console.WriteLine("O resultado é: {0}", resultado);
        }

        public static EnumOperadores LerOperadorMath()
        {
            while (true)
            {
                switch(RequestInput("Escreva um Operador: "))
                {
                    case "+":
                        return EnumOperadores.soma;
                    case "-":
                        return EnumOperadores.subtracao;
                    case "*":
                        return EnumOperadores.multiplicacao;
                    case "/":
                        return EnumOperadores.divisao;
                }
            }
        }

        public static void EscreveListaDeNumeros(List<int> lista)
        {
            StringBuilder s = new StringBuilder();
            foreach (var item in lista)
            {
                s.Append(item + ",");
            }

            ShowResultado(s.Remove(s.Length - 1, 1));

        }

    }
}
