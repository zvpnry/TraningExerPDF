﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainningPdf.Math
{
    internal interface ICalculate
    {

        decimal Calcula(decimal num1, decimal num2,EnumOperadores operador);
    }
}
