﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainningPdf.Math
{
    internal class Calculate : ICalculate
    {
        public decimal Calcula(decimal num1, decimal num2, EnumOperadores operador)
        {
            switch(operador)
            {
                case EnumOperadores.divisao:
                    return num1 / num2;
                case EnumOperadores.multiplicacao:
                    return num1 * num2;
                case EnumOperadores.soma:
                    return num1 + num2;
                case EnumOperadores.subtracao:
                    return num1 - num2;
            }
            throw new Exception("Falta operador identificado no enumerador.");
        }        
    }
}
