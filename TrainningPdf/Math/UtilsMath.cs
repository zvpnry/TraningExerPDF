﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainningPdf.Math
{
    public enum NumberType
    {
        pares,
        impares
    }
   public static class UtilsMath
    {
        public static List<int> ParOuImpares(int valorInicial,int valorFinal,NumberType type)
        {
            List<int> result = new List<int>();
            int operationResult = type == NumberType.pares ? 0 : 1;
            if (valorInicial > valorFinal)
                throw new Exception("Valor inicial não pode ser maior que valor Final");
            for (int i = valorInicial; i <= valorFinal; i++)
            {
                if (i % 2 == operationResult)
                    result.Add(i);
            }
            return result;

        }

        public static decimal GetMinNumber(List<decimal> Numbers)
        {
            return Numbers.Min();
        }

        public static decimal GetMaxNumber(List<decimal> Numbers)
        {
            return Numbers.Max();
        }
    }
}
